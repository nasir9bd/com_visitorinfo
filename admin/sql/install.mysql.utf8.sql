CREATE TABLE IF NOT EXISTS `#__visitorinfo` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `browser_name` VARCHAR(30),
  `browser_version` VARCHAR(100),
  `ip_address` VARCHAR(50),
  `platform` VARCHAR(100),
  `user_agent` VARCHAR(300),
  `city` VARCHAR(100),
  `country` VARCHAR(50),
  `time` DATETIME,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
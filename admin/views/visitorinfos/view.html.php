<?php

// No direct access to this file
defined('_JEXEC') or die;


 //VisitorInfos View

class VisitorInfoViewVisitorInfos extends JViewLegacy
{

    function display($tpl = null)
    {
        // Get data from the model
        $this->items		= $this->get('Items');
        $this->pagination	= $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JError::raiseError(500, implode('<br />', $errors));

            return false;
        }
        $this->addToolbar();

        // Display the template
        parent::display($tpl);
    }

    protected function addToolbar(){
        JToolbarHelper::title(JText::_('COM_VISITORINFO_TOOLBAR_TITLE'));
    }
}
<?php
defined('_JEXEC') or die;
?>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th><?php echo JText::_('COM_VISITORINFO_TIME');?></th>
            <th><?php echo JText::_('COM_VISITORINFO_IPADDRESS');?></th>
            <th><?php echo JText::_('COM_VISITORINFO_BROWSER');?></th>
            <th><?php echo JText::_('COM_VISITORINFO_PLATFORM');?></th>
            <th><?php echo JText::_('COM_VISITORINFO_LOCATION');?></th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="5">
                <?php echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
    </tfoot>
    <tbody>
        <?php if (!empty($this->items)) : ?>
        <?php foreach ($this->items as $i => $row) : ?>
        <tr>

            <td>
                <?php echo $row->time; ?>
            </td>
            <td>
                <?php echo $row->ip_address; ?>
            </td>
            <td>
                <?php echo $row->browser_name,", ",$row->browser_version; ?>
            </td>
            <td>
                <?php echo $row->platform; ?>
            </td>
            <td>
                <?php echo $row->city,", ",$row->country; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
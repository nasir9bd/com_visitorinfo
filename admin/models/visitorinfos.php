<?php

// No direct access to this file
defined('_JEXEC') or die;

/**
 * VisitorInfoList Model
 *
 */
class VisitorInfoModelVisitorInfos extends JModelList
{
    /**
     * Method to build an SQL query to load the list data.
     *
     * @return  An SQL query
     */
    protected function getListQuery()
    {
        // Initialize variables.
        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);

        // Create the base select statement.
        $query->select('*')
            ->from($db->quoteName('#__visitorinfo'));

        return $query;
    }
}
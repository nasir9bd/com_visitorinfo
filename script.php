<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


class Com_visitorinfoInstallerScript
{
    /**
     * method to install the component
     *
     * @return void
     */
    function install($parent)
    {
        // installing plugin
        $manifest = $parent->get("manifest");
        $parent = $parent->getParent();
        $source = $parent->getPath("source");
        $installer = new JInstaller();

        foreach($manifest->plugins->plugin as $plugin) {
            $attributes = $plugin->attributes();
            $plg = $source . DIRECTORY_SEPARATOR . $attributes['folder'] . DIRECTORY_SEPARATOR . $attributes['plugin'];
            $installer->install($plg);
        }

        // $parent is the class calling this method
        $parent->getParent()->setRedirectURL('index.php?option=com_visitorinfo');
    }

    /**
     * method to uninstall the component
     *
     * @return void
     */
    function uninstall($parent)
    {
        $manifest = $parent->get("manifest");
        $parent = $parent->getParent();
        $source = $parent->getPath("source");

        $installer = new JInstaller();
        $db	= JFactory::getDBO();

        // Uninstall plugins
        foreach($manifest->plugins->plugin as $plugin) {
            $attributes = $plugin->attributes();
            $db->setQuery("select extension_id from #__extensions where name = '".$attributes['name']."' and type = 'plugin' and element = '".$attributes['plugin']."'");
            $plugin = $db->loadObject();
            $installer->uninstall('plugin',$plugin->extension_id);
        }

        echo '<p>' . JText::_('COM_VISITORINFO_UNINSTALL_TEXT') . '</p>';
    }

    /**
     * method to update the component
     *
     * @return void
     */
    function update($parent)
    {
        // $parent is the class calling this method
        echo '<p>' . JText::sprintf('COM_VISITORINFO_UPDATE_TEXT', $parent->get('manifest')->version) . '</p>';
    }

    /**
     * method to run before an install/update/uninstall method
     *
     * @return void
     */
    function preflight($type, $parent)
    {
        // $parent is the class calling this method
        // $type is the type of change (install, update or discover_install)
        echo '<p>' . JText::_('COM_VISITORINFO_PREFLIGHT_' . $type . '_TEXT') . '</p>';
    }

    /**
     * method to run after an install/update/uninstall method
     *
     * @return void
     */
    function postflight($type, $parent)
    {
        // $parent is the class calling this method
        // $type is the type of change (install, update or discover_install)
        echo '<p>' . JText::_('COM_VISITORINFO_POSTFLIGHT_' . $type . '_TEXT') . '</p>';
    }
}
<?php
defined('_JEXEC') or die;

class plgSystemVisitorinfo extends JPlugin{
    public function onAfterInitialise(){
        jimport('joomla.environment.browser');
        $browser = new JBrowser();
        $db = JFactory::getDbo();
        $date = new JDate();
        $current_datetime = $date->toSql();
        $ip = filter_var($this->getIP(), FILTER_VALIDATE_IP);
        $location = $this->getLocation($ip);
        $city = $location->city;
        $country = $location->country;
        $sql = "INSERT INTO `#__visitorinfo` (
                    `browser_name`,
                    `browser_version`,
                    `ip_address`,
                    `platform`,
                    `user_agent`,
                    `city`,
                    `country`,
                    `time`
                    )VALUES (
                    '".$browser->getBrowser()."',
                    '".$browser->getVersion()."',
                    '".$ip."',
                    '".$browser->getPlatform()."',
                    '".$browser->getAgentString()."',
                    '".$city."',
                    '".$country."',
                    '".$current_datetime."'
                    )";
        $db->setQuery($sql);
        $db->query();

    }

    public function getIP()
    {
        // Try REMOTE_ADDR
        if (isset($_SERVER['REMOTE_ADDR']) and $_SERVER['REMOTE_ADDR'] != '')
        {
            return $_SERVER['REMOTE_ADDR'];
        }
        // Fall back to HTTP_CLIENT_IP
        elseif (isset($_SERVER['HTTP_CLIENT_IP']) and $_SERVER['HTTP_CLIENT_IP'] != '')
        {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        // Finally fall back to HTTP_X_FORWARDED_FOR
        // I'm aware this can sometimes pass the users LAN IP, but it is a last ditch attempt
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) and $_SERVER['HTTP_X_FORWARDED_FOR'] != '')
        {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        // Nothing? Return false
        return false;
    }

    public function getLocation($ip){
        $request_body = "http://ip-api.com/json/" . $ip;
        $location = file_get_contents($request_body);
        return json_decode($location);
    }
}